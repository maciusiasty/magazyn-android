package maciek.com.magazyn;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import maciek.com.fragmenty.kontaktFragment;
import maciek.com.fragmenty.oNasFragment;
import maciek.com.tematyczne.dodajProduktActivity;
import maciek.com.tematyczne.kupProduktActivity;
import maciek.com.tematyczne.mapsActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button oNas_button,kontakt_button,kup_button,administrator_button,znajdzNas;
    private Button takDialog,nieDialog;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initButton();

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setLogo(R.drawable.barimage);
        setSupportActionBar(toolbar);

        context = getApplicationContext();


    }




    private void changeFragment(View view)
    {
        Fragment fragment;
        switch(view.getId())
        {
            case R.id.button_Kontkat:
                fragment = new kontaktFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentDane,fragment);
                fragmentTransaction.commit();
                break;
            case R.id.button_oNas:
                fragment = new oNasFragment();
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.fragmentDane,fragment);
                fragmentTransaction2.commit();
                break;
            default:
                Toast.makeText(getApplicationContext(),"Blad wybory opcji !",Toast.LENGTH_LONG).show();
        }
    }

    private void initButton()
    {
        oNas_button = (Button) findViewById(R.id.button_oNas);
        kontakt_button = (Button) findViewById(R.id.button_Kontkat);
        kup_button = (Button) findViewById(R.id.button_kupProdukt);
        administrator_button = (Button) findViewById(R.id.button_administrator);
        kontakt_button.setOnClickListener(this);
        oNas_button.setOnClickListener(this);
        kup_button.setOnClickListener(this);
        administrator_button.setOnClickListener(this);
        znajdzNas = (Button) findViewById(R.id.button_zajdzNas);
        znajdzNas.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.button_kupProdukt:
                Intent intentKuProdukt = new Intent(getApplicationContext(),kupProduktActivity.class);
                startActivity(intentKuProdukt);
                break;
            case R.id.button_Kontkat:
                changeFragment(v);
                break;
            case R.id.button_administrator:
                Intent intentAdministrator = new Intent(getApplicationContext(),dodajProduktActivity.class);
                startActivity(intentAdministrator);
                break;
            case R.id.button_oNas:
            changeFragment(v);
                break;
            case R.id.button_zajdzNas:
                OknoDialogoweGPS();
                break;
            default:
                Toast.makeText(getApplicationContext(),"Bląd przy wyborze opcji! ",Toast.LENGTH_SHORT).show();
        }

    }



    /*
    @Description okno możliowści wlączenia GPS'a
     */
    private void OknoDialogoweGPS()
    {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setTitle("Dostęp do GPS");
        dialog.setContentView(R.layout.dostep_dogps_layout);
        nieDialog = (Button) dialog.findViewById(R.id.nieDialog);
        takDialog = (Button) dialog.findViewById(R.id.takDialog);

        nieDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });




        takDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(MainActivity.this,mapsActivity.class);
                startActivity(intent);

            }
        });

        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        item.setIcon(R.drawable.jsonimage);
        switch (item.getItemId())
        {
            case R.id.jsonImage:
                Toast.makeText(context,"Wybrano bazę typu Json",Toast.LENGTH_SHORT).show();
                sqlCzyJson czyJson = sqlCzyJson.getInstance();
                czyJson.czyJson = true;
                break;
            case R.id.sqlImage:
                Toast.makeText(context,"Wybrano bazę typu SQL",Toast.LENGTH_SHORT).show();
                sqlCzyJson czySQL = sqlCzyJson.getInstance();
                czySQL.czyJson = false;
                break;
            default:
                Toast.makeText(context,"Blad przy akcji ActionBar",Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
}
