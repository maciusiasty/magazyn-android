package maciek.com.magazyn;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by root on 29.12.16.
 */

public class ProduktArrayAdapter extends ArrayAdapter<Produkt> {


    private final Context context;
    private final List<Produkt> values;

    public ProduktArrayAdapter(Context context, List<Produkt> objects) {
        super(context,R.layout.mylist_kupprodukt, objects);

        this.context = context;
        this.values = objects;

    }



    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

         Produkt produkt = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.mylist_kupprodukt,parent,false);
        }

        TextView cena = (TextView) convertView.findViewById(R.id.cena);
        TextView iloscProduktu = (TextView) convertView.findViewById(R.id.iloscProduktu);
        TextView nazwaProduktu = (TextView) convertView.findViewById(R.id.nazwaProduktu);
        ImageView obraz = (ImageView) convertView.findViewById(R.id.zdjeciePomocniczne);


        cena.setText(produkt.getCena().toString());
        iloscProduktu.setText(produkt.getIloscProduktu().toString());
        nazwaProduktu.setText(produkt.getNazwaProduktu().toString());
        Drawable drawable = convertView.getResources().getDrawable(R.drawable.ic_launcher);
        obraz.setImageDrawable(drawable);

        return convertView;
    }
}
