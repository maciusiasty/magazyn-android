package maciek.com.magazyn;

import android.database.Cursor;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by root on 29.12.16.
 */

public class Produkt {

    private Integer Id;
    private String nazwaProduktu;
    private Integer iloscProduktu;
    private Double cena;

    /*
    @Descroption Konstruktor pobierajacy plik json'a
     */


    public Produkt(JSONObject jsonObject) {
        try {
            this.cena = jsonObject.getDouble("cena");
            this.nazwaProduktu = jsonObject.getString("nazwa");
            this.iloscProduktu = jsonObject.getInt("ilosc");

        } catch (JSONException JsonEx) {
            JsonEx.printStackTrace();
        }

    }


    /*
    @Description Dla klasy Cursor - Konstruktor dla tworzenia bazy danych
     */

    public Produkt(Cursor cursor)
    {
        Id = cursor.getInt(0);
        nazwaProduktu = cursor.getString(1);
        cena = cursor.getDouble(2);
        iloscProduktu = cursor.getInt(3);
    }

    /*
    @Description Domyslny konstruktor
     */

    public Produkt(Double cena, Integer iloscProduktu, String nazwaProduktu) {
        this.cena = cena;
        this.iloscProduktu = iloscProduktu;
        this.nazwaProduktu = nazwaProduktu;
    }

    public Double getCena() {
        return cena;
    }



    public void setCena(Double cena) {
        this.cena = cena;
    }

    public Integer getIloscProduktu() {
        return iloscProduktu;
    }

    public void setIloscProduktu(Integer iloscProduktu) {
        this.iloscProduktu = iloscProduktu;
    }

    public String getNazwaProduktu() {
        return nazwaProduktu;
    }

    public void setNazwaProduktu(String nazwaProduktu) {
        this.nazwaProduktu = nazwaProduktu;
    }


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }
}