package maciek.com.bazaSQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by root on 06.01.17.
 */

public class BazaHandler extends SQLiteOpenHelper {



    public static final String NAZWA_TABELI = "Produkty";
    public static final int WERSJA_TABELI = 1;


    public static final String ID = "Id";
    public static final String NAZWA_PRODUKTU = "nazwa_produktu";
    public static final String ILOSC_PRODUKTU = "ilosc_produktu";
    public static final String CENA_PRODUKTU = "cena_produktu";

    public static final String[] tabelaWartosci = {ID,NAZWA_PRODUKTU,CENA_PRODUKTU,ILOSC_PRODUKTU};



    public BazaHandler(Context context) {
        super(context,NAZWA_TABELI,null,WERSJA_TABELI);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String stworz_baze = "CREATE TABLE "+NAZWA_TABELI+"("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+NAZWA_PRODUKTU+" TEXT NOT NULL,"+CENA_PRODUKTU+" DOUBLE NOT NULL,"+ILOSC_PRODUKTU+" DOUBLE NOT NULL);";
        db.execSQL(stworz_baze);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String upgrade = "DROP TABLE IF EXISTS " + NAZWA_TABELI;

        db.execSQL(upgrade);
    }
}
