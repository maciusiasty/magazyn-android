package maciek.com.bazaSQL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import maciek.com.magazyn.Produkt;

/**
 * Created by root on 06.01.17.
 */

public class ProduktSqlDAOImpl implements ProduktSqlDAO {

    private BazaHandler myDatabase;
    private SQLiteDatabase sqLiteOpenHelper;

   public ProduktSqlDAOImpl(Context context)
    {
        this.myDatabase = new BazaHandler(context);

    }

    public void open()
    {
        sqLiteOpenHelper = myDatabase.getWritableDatabase();

    }

    private void close()
    {
        sqLiteOpenHelper.close();
    }

    @Override
    public long addProdukt(Produkt produkt) {

        ContentValues contentValues = new ContentValues();

        contentValues.put(BazaHandler.NAZWA_PRODUKTU,produkt.getNazwaProduktu());
        contentValues.put(BazaHandler.CENA_PRODUKTU,produkt.getCena());
        contentValues.put(BazaHandler.ILOSC_PRODUKTU,produkt.getIloscProduktu());

        long czySukces = sqLiteOpenHelper.insert(BazaHandler.NAZWA_TABELI,null,contentValues);
        sqLiteOpenHelper.close();
        return czySukces;
    }

    @Override
    public Produkt getProdukt(int Id) {

        ContentValues contentValues = new ContentValues();

        Cursor cursor = sqLiteOpenHelper.query(BazaHandler.NAZWA_TABELI,BazaHandler.tabelaWartosci,BazaHandler.ID+" = "+Id,null,null,null,null);

        cursor.moveToFirst();
        Produkt produkt = new Produkt(cursor);

        return produkt;
    }

    @Override
    public ArrayList<Produkt> getListOfProdukt() {
        ArrayList<Produkt> produkts = new ArrayList<Produkt>();

        String myQuery = "SELECT * FROM "+BazaHandler.NAZWA_TABELI;
        sqLiteOpenHelper = myDatabase.getReadableDatabase();
        Cursor cursor = sqLiteOpenHelper.rawQuery(myQuery,null);
        if(cursor.moveToFirst()) {
           do {
                Produkt produkt = new Produkt(cursor);
                produkts.add(produkt);
                cursor.moveToNext();
            } while(cursor.moveToNext());
        }
        return produkts;
    }

    @Override
    public void deleteProdukt(int Id) {

        sqLiteOpenHelper.delete(BazaHandler.NAZWA_TABELI,Id + " = "+Id,null);

    }

    @Override
    public void modifyProdukt(Produkt produkt) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(BazaHandler.NAZWA_PRODUKTU,produkt.getNazwaProduktu());
        contentValues.put(BazaHandler.CENA_PRODUKTU,produkt.getCena());
        contentValues.put(BazaHandler.ILOSC_PRODUKTU,produkt.getIloscProduktu());


        sqLiteOpenHelper.replace(BazaHandler.NAZWA_TABELI,null,contentValues);

    }
}
