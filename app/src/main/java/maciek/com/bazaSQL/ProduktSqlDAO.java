package maciek.com.bazaSQL;

import java.util.ArrayList;

import maciek.com.magazyn.Produkt;

/**
 * Created by root on 06.01.17.
 */

public interface ProduktSqlDAO {


    long addProdukt(Produkt produkt);
    Produkt getProdukt(int Id);
    ArrayList<Produkt> getListOfProdukt();
    void deleteProdukt(int Id);
    void modifyProdukt(Produkt produkt);
    void open();

}
