package maciek.com.tematyczne;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import maciek.com.bazaSQL.ProduktSqlDAO;
import maciek.com.bazaSQL.ProduktSqlDAOImpl;
import maciek.com.jsonFile.ProduktDAO;
import maciek.com.jsonFile.ProduktDAOImpl;
import maciek.com.magazyn.MainActivity;
import maciek.com.magazyn.Produkt;
import maciek.com.magazyn.R;
import maciek.com.magazyn.sqlCzyJson;

/**
 * Created by root on 29.12.16.
 */

public class dodajProduktActivity extends AppCompatActivity implements View.OnClickListener {

    private Button produktDodaj,zamknijDodaj;
    private EditText cenaDodaj,iloscDodaj,nazwaProduktuDodaj;

    private Context context;

    private sqlCzyJson czyJson;

    private ProduktDAO produktDAO;

    private ProduktSqlDAO produktSqlDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodajprodukt_layout);
        initButton();

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setLogo(R.drawable.barimage);
        setSupportActionBar(toolbar);

        context = getApplicationContext();

        produktDAO = new ProduktDAOImpl();


    }


   /*
    @Description inicjuje zmienne do kontroler
     */
    private void initButton()
    {
        cenaDodaj = (EditText) findViewById(R.id.cenaDodaj);
        iloscDodaj = (EditText) findViewById(R.id.cenaDodaj);
        nazwaProduktuDodaj = (EditText) findViewById(R.id.nazwaProduktuDodaj);
        produktDodaj = (Button) findViewById(R.id.produktDodaj);
        zamknijDodaj = (Button) findViewById(R.id.zamknijDodaj);
        produktDodaj.setOnClickListener(this);
        zamknijDodaj.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.produktDodaj:
            dodajProdukt();
                break;
            case R.id.zamknijDodaj:
                Intent intent = new Intent(dodajProduktActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            default:
                Toast.makeText(getApplicationContext(),"Blad wyboru menu ! ",Toast.LENGTH_SHORT).show();
        }

    }


    /*
    @Description dodawanie Produktu do bazy json czy sql
     */

    private void dodajProdukt()
    {
        Produkt produkt = new Produkt(Double.parseDouble(cenaDodaj.getText().toString()),Integer.parseInt(iloscDodaj.getText().toString()),nazwaProduktuDodaj.getText().toString());

        czyJson = sqlCzyJson.getInstance();

      if(czyJson.czyJson) {


          if (produktDAO.toJson(getApplicationContext(), produkt)) {
              Toast.makeText(getApplicationContext(), "Dodano dane do bazy danych ", Toast.LENGTH_SHORT).show();
              wyczyscPola();
          } else
              Toast.makeText(getApplicationContext(), "Blad przy dodawaniu danych do bazy danych", Toast.LENGTH_LONG).show();
      } else if(!czyJson.czyJson)
      {
          produktSqlDAO = new ProduktSqlDAOImpl(context);
          produktSqlDAO.open();
          if(produktSqlDAO.addProdukt(produkt) >= 0) {
              Toast.makeText(context, "Dodano dane do bazy SQL", Toast.LENGTH_SHORT).show();
              wyczyscPola();
          }
          else Toast.makeText(context,"Blad przy dodawaniu danych do bazy danych",Toast.LENGTH_SHORT).show();

      }
    }

    private void wyczyscPola()
    {
        cenaDodaj.setText("");
        iloscDodaj.setText("");
        nazwaProduktuDodaj.setText("");
    }

}
