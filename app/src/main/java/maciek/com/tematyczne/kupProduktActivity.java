package maciek.com.tematyczne;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import maciek.com.bazaSQL.ProduktSqlDAO;
import maciek.com.bazaSQL.ProduktSqlDAOImpl;
import maciek.com.jsonFile.ProduktDAO;
import maciek.com.jsonFile.ProduktDAOImpl;
import maciek.com.magazyn.MainActivity;
import maciek.com.magazyn.Produkt;
import maciek.com.magazyn.ProduktArrayAdapter;
import maciek.com.magazyn.R;
import maciek.com.magazyn.sqlCzyJson;


/**
 * Created by root on 31.12.16.
 */

public class kupProduktActivity extends AppCompatActivity {

    private Context context;
    private ListView listView;
    private Button zamknijKupProdukt;

    private ProduktDAO produktDAO;
    private ProduktSqlDAO produktSqlDAO;

    private ArrayList<Produkt> arrayProdukt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kupprodukt);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setLogo(R.drawable.barimage);
        setSupportActionBar(toolbar);

        context = getApplicationContext();

        pobierzListe(context);


        ProduktArrayAdapter produktArrayAdapter = new ProduktArrayAdapter(context,arrayProdukt);
        listView = (ListView) findViewById(R.id.myListView);
        listView.setAdapter(produktArrayAdapter);

        //Dodaje przyciski
        initButton();


    }

    /*
    @Description Dodaje przyscisk oraz akcje do przycisku
     */
    private void initButton()
    {
        zamknijKupProdukt = (Button) findViewById(R.id.zamknijKupProdukt);
        zamknijKupProdukt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(kupProduktActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }
    /*
    @Description pobiera liste z bazy danych SQL lub JSON
     */
    private void pobierzListe(Context context)
    {
        sqlCzyJson czyJson = sqlCzyJson.getInstance();
        produktSqlDAO = new ProduktSqlDAOImpl(context);

        if(czyJson.czyJson) {
            produktDAO = new ProduktDAOImpl();
            arrayProdukt = produktDAO.fromJson(context);
        }
        else {
            produktSqlDAO.open();
            arrayProdukt = produktSqlDAO.getListOfProdukt();
        }


    }



}
