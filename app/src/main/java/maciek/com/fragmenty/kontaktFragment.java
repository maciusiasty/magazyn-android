package maciek.com.fragmenty;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import maciek.com.magazyn.R;

/**
 * Created by root on 30.12.16.
 */

public class kontaktFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.kontakt_layout,container,false);
    }
}
