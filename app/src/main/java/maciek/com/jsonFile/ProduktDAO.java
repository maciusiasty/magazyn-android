package maciek.com.jsonFile;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;

import maciek.com.magazyn.Produkt;

/**
 * Created by root on 29.12.16.
 */

public interface ProduktDAO {

    public ArrayList<Produkt> fromJsonToArray(JSONArray jsonObject);

    public ArrayList<Produkt> fromJson(Context context);

    public boolean toJson(Context context,Produkt produkt);

}
