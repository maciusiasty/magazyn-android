package maciek.com.jsonFile;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import maciek.com.magazyn.Produkt;

/**
 * Created by root on 29.12.16.
 */

public class ProduktDAOImpl implements ProduktDAO {

    /*
    @Description Konwertujacy plik json'a do kolekcji arrayList
     */

    @Override
    public  ArrayList<Produkt> fromJsonToArray(JSONArray jsonArray) {
        ArrayList<Produkt> produkts = new ArrayList<Produkt>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                produkts.add(new Produkt(jsonArray.getJSONObject(i)));
            } catch (JSONException jsonEx) {
                jsonEx.printStackTrace();
            }

        }
        return produkts;
    }

    /*
    @Description Pobiera z jsona dane
     */
    @Override
    public ArrayList<Produkt> fromJson(Context context) {
        ArrayList<Produkt> produkty = new ArrayList<Produkt>();
        File path = context.getFilesDir();
        File file = new File(path,"baza.json");
        try {
            String doParsowania = readFile(file.getAbsolutePath());
            int beginIndex = 0;
            for(int i=0;i<doParsowania.length();i++)
            {
                if(doParsowania.charAt(i)=='}')
                {
                    String nowy = doParsowania.substring(beginIndex,i+1);
                    beginIndex = i+1;
                    JSONObject jsonObject = new JSONObject(nowy);
                    produkty.add(new Produkt(jsonObject));

                }


            }
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }

        return produkty;
    }


    /*
    @Description czytanie pliku (do odczytu jsona)
     */

    public static String readFile(String filename) {
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
    @Description Dodaje do jsona dane
     */
    @Override
    public boolean toJson(Context context,Produkt produkt)
    {
        boolean czySukces = true;
        JSONObject jsonObject = new JSONObject();
        FileOutputStream stream=null;
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonTemp = new JSONObject();


        try {

            jsonTemp.put("cena", produkt.getCena());
            jsonTemp.put("ilosc",produkt.getIloscProduktu());
            jsonTemp.put("nazwa",produkt.getNazwaProduktu().toString());
        }
        catch(JSONException jsonException)
        {
            jsonException.printStackTrace();
        }

        try
        {
            File path = context.getFilesDir();
            File file = new File(path, "baza.json");
            if(!file.exists())
                file.createNewFile();

            stream = new FileOutputStream(file,true);

            stream.write(jsonTemp.toString().getBytes());




        }
        catch(IOException ioException)
        {
            czySukces = false;
            ioException.printStackTrace();

        }




        return czySukces;



    }



}
